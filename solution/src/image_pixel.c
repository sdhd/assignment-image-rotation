//
// Created by adgjw on 25.10.2022.
//
#include <malloc.h>
#include <stdint.h>


struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void init_image(struct image* img, uint64_t width, uint64_t height){
    img->width=width;
    img->height=height;
    img->data=malloc(sizeof(struct pixel) * img->height*img->width);
}

void del_image(struct image const* img){
    free(img->data);
}
