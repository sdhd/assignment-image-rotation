//
// Created by adgjw on 25.10.2022.
//
#include "image_pixel.h"


static int set_pixel(struct image const* source, int i, int j){
    return (int)source->height * ((int)source->width - j - 1) + i;
}

static int get_pixel(struct image const* source, int i, int j){
    return i * (int)source->width + j;
}

struct image rotate(struct image const* source){
    struct image tmp;

    init_image(&tmp, source->height, source->width);

    for(int i = 0; i < source->height;++i)
        for(int j = 0; j < source->width;++j) {
            tmp.data[set_pixel(source, i, j)] = source->data[get_pixel(source, i, j)];
        }
    del_image(source);
    return tmp;
}
